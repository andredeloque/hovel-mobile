import 'package:flutter/material.dart';
import 'package:hovel/tabs/home_tab.dart';
import 'package:hovel/tabs/products_tab.dart';
import 'package:hovel/widgets/custom_drawer.dart';
import 'package:hovel/Pedido/Pedido.dart';
import 'package:hovel/Pedido/Historico.dart';

import '../Pedido/Historico.dart';

class HomeScreen extends StatelessWidget {
  final _pageController = PageController();

  @override
  Widget build(BuildContext context) {
    return PageView(
      controller: _pageController,
      physics: NeverScrollableScrollPhysics(),
      children: <Widget>[
        Scaffold(
          body: HomeTab(),
          drawer: CustomDrawer(_pageController),
        ),
        Scaffold(
          appBar: AppBar(
            title: Text("Pedidos"),
            centerTitle: true,
          ),
          drawer: CustomDrawer(_pageController),
          body: Pedido(),
        ),
        Scaffold(
          appBar: AppBar(
            title: Text("Histórico"),
            centerTitle: true,
          ),
          drawer: CustomDrawer(_pageController),
          body: Historico(),
        ),
      ],
    );
  }
}
