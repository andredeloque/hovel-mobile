import 'package:flutter/material.dart';
import 'package:hovel/Customize/ConfirmDialog.dart';

Future<ConfirmAction> CustomDialog(BuildContext context, String title) async {
  return showDialog<ConfirmAction>(
    context: context,
    barrierDismissible: false,
    builder: (BuildContext context) {
      return AlertDialog(
        title: Center(child: Text(title)),
        actions: <Widget>[
          Row(
            children: <Widget>[
              FlatButton(
                color: Colors.redAccent,
                child: Text("Não"),
                textColor: Colors.white,
                onPressed: () {
                  Navigator.of(context).pop(ConfirmAction.CANCEL);
                },
              ),
              Container(width: 10.0),
              FlatButton(
                color: Colors.blueAccent,
                child: Text("Sim"),
                textColor: Colors.white,
                onPressed: () {
                  Navigator.of(context).pop(ConfirmAction.ACCEPT);
                },
              ),
            ],
          ),
        ],
      );
    },
  );
}