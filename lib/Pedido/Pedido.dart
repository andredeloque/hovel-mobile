import 'package:flutter/material.dart';
import 'package:hovel/Customize/ConfirmDialog.dart';
import 'package:hovel/Customize/CustomizeDialog.dart';
import 'package:hovel/Pedido/Historico.dart';


class Pedido extends StatefulWidget {
  @override
  _PedidoState createState() => _PedidoState();
}

class _PedidoState extends State<Pedido> {

  List<DropdownMenuItem<String>> listMesa      = [];
  List<DropdownMenuItem<String>> listPessoa    = [];
  List<DropdownMenuItem<String>> listTipo      = [];
  List<DropdownMenuItem<String>> listQtde      = [];
  List<DropdownMenuItem<String>> listCate      = [];
  List<DropdownMenuItem<String>> listProduto   = [];

  String selecionadoMesa      = null;
  String selecionadoPessoa    = null;
  String selecionadoQtde      = null;
  String selecionadoProduto   = null;
  String tipoCategoria        = null;
  int pedido               = 0;

  List _listaPedido = [];

  void dadosMesa() {
    listMesa = [];
    listMesa.add(DropdownMenuItem(
      child: Text("Mesa 1"),
      value: "1"
    ));
    listMesa.add(DropdownMenuItem(
        child: Text("Mesa 2"),
        value: "2"
    ));
    listMesa.add(DropdownMenuItem(
        child: Text("Mesa 3"),
        value: "3"
    ));
    listMesa.add(DropdownMenuItem(
        child: Text("Mesa 4"),
        value: "4"
    ));
    listMesa.add(DropdownMenuItem(
        child: Text("Mesa 5"),
        value: "5"
    ));
  }

  void dadosPessoas() {
    listPessoa = [];
    listPessoa.add(DropdownMenuItem(
      child: Text("1"),
      value: "1",
    ));
    listPessoa.add(DropdownMenuItem(
        child: Text("2"),
        value: "2"
    ));
    listPessoa.add(DropdownMenuItem(
      child: Text("3"),
      value: '3'
    ));
    listPessoa.add(DropdownMenuItem(
      child: Text("4"),
      value: "4"
    ));
    listPessoa.add(DropdownMenuItem(
      child: Text("5 +"),
      value: "5"
    ));
  }

  void dadosQtde() {
    listQtde = [];
    listQtde.add(DropdownMenuItem(
      child: Text("1"),
      value: "1"
    ));
    listQtde.add(DropdownMenuItem(
        child: Text("2"),
        value: "2"
    ));
    listQtde.add(DropdownMenuItem(
        child: Text("3"),
        value: "3"
    ));
    listQtde.add(DropdownMenuItem(
        child: Text("4"),
        value: "4"
    ));
  }

  void dadosCategoria() {
    listCate = [];
    listCate.add(DropdownMenuItem(
        child: Text("Bebidas"),
        value: "Bebidas"
    ));
    listCate.add(DropdownMenuItem(
        child: Text("Porção"),
        value: "Porção"
    ));
    listCate.add(DropdownMenuItem(
        child: Text("Lanche"),
        value: "Lanche"
    ));
  }

  void dadosProduto() {
    if (tipoCategoria == "Bebidas") {
      listProduto = [];
      listProduto.add(DropdownMenuItem(
          child: Text("Suco de Laranja"),
          value: "Suco de Laranja"
      ));
      listProduto.add(DropdownMenuItem(
          child: Text("Coca-cola"),
          value: "Coca-cola"
      ));
      listProduto.add(DropdownMenuItem(
          child: Text("Suco de Melancia"),
          value: "Suco de Melancia"
      ));
    } else
      if (tipoCategoria == "Porção") {
        listProduto = [];
        listProduto.add(DropdownMenuItem(
            child: Text("Batata Frita"),
            value: "Batata Frita"
        ));
        listProduto.add(DropdownMenuItem(
            child: Text("Frios"),
            value: "Frios"
        ));
        listProduto.add(DropdownMenuItem(
            child: Text("Peixe"),
            value: "Peixe"
        ));
      } else
          if (tipoCategoria == "Lanche") {
            listProduto = [];
            listProduto.add(DropdownMenuItem(
                child: Text("X- Tudo"),
                value: "X- Tudo"
            ));
            listProduto.add(DropdownMenuItem(
                child: Text("X- Calabresa"),
                value: "X- Calabresa"
            ));
            listProduto.add(DropdownMenuItem(
                child: Text("X- Bacon"),
                value: "X- Bacon"
            ));
          }
  }

  void addPedido() {
    if ((selecionadoQtde == null) || (selecionadoMesa == null) || (selecionadoPessoa == null) || (selecionadoProduto == null) || (tipoCategoria == null)) {
      AlertDialog dialog = AlertDialog(
        backgroundColor: Colors.white,
        title: Text("Campos vazios", textAlign: TextAlign.center, style: TextStyle(fontSize: 15,),),
        actions: <Widget>[
          FlatButton(
            color: Colors.blueAccent,
            child: Text("OK"),
            onPressed: (){
              Navigator.pop(context);
            },
          ),
          Padding(
            padding: EdgeInsets.fromLTRB(0, 0, 90, 0),
          ),
        ],
      );
      showDialog(context: context,child: dialog);
    } else {
      pedido = pedido + 1;
      setState(() {
        Map<String, dynamic> novoPedido = Map();
        novoPedido["pedido"]      = pedido.toString();
        novoPedido["quantidade"]  = selecionadoQtde;
        novoPedido["mesa"]        = selecionadoMesa;
        novoPedido["pessoa"]      = selecionadoPessoa;
        novoPedido["produto"]     = selecionadoProduto;
        novoPedido["categoria"]   = tipoCategoria;

        _listaPedido.add(novoPedido);

      });
    }
  }

  Future<void> _apagarItensPedido() async {
    setState(() {
      _listaPedido.clear();
    });
  }

  void enviarDados() async{
    if ((selecionadoQtde == null) || (selecionadoMesa == null) || (selecionadoPessoa == null) || (selecionadoProduto == null) || (tipoCategoria == null)) {
      AlertDialog dialog = AlertDialog(
        backgroundColor: Colors.white,
        title: Text("Informe os dados corretamente", textAlign: TextAlign.center, style: TextStyle(fontSize: 18,),),
        actions: <Widget>[
          FlatButton(
            color: Colors.blueAccent,
            child: Text("OK"),
            onPressed: () {
              Navigator.pop(context);
            },
          ),
          Padding(
            padding: EdgeInsets.fromLTRB(0, 0, 90, 0),
          ),
        ],
      );
      showDialog(context: context, child: dialog);
    } else
        if (_listaPedido.length < 1) {
          AlertDialog dialog = AlertDialog(
            backgroundColor: Colors.white,
            title: Text("Lista vazia", textAlign: TextAlign.center, style: TextStyle(fontSize: 18,),),
            actions: <Widget>[
              FlatButton(
                color: Colors.blueAccent,
                child: Text("OK"),
                onPressed: () {
                  Navigator.pop(context);
                },
              ),
              Padding(
                padding: EdgeInsets.fromLTRB(0, 0, 90, 0),
              ),
            ],
          );
          showDialog(context: context, child: dialog);
        } else {
          ConfirmAction actionEnvio = await CustomDialog(context,
              "Deseja enviar todos os itens?");
          if (actionEnvio == ConfirmAction.ACCEPT) {
              Navigator.push(context, MaterialPageRoute(builder: (context) => Historico(dados: _listaPedido,)));
              //_apagarItensPedido();
          }
        }
  }

  @override
  Widget build(BuildContext context) {
    dadosMesa();
    dadosPessoas();
    dadosQtde();
    dadosCategoria();
    dadosProduto();
    return 
    /*Scaffold(
      appBar: AppBar(
        title: Text("Pedido"),
        backgroundColor: Color(0xff36B3C9),
      ),
      body: */
      Container(
            padding: EdgeInsets.all(20),
            child: Column(
              children: [
                Container(
                  child: Row(
                    children: [
                      SizedBox(
                        height: 10,
                      ),
                      Expanded(
                        child:DropdownButton(
                          value: selecionadoMesa,
                          items: listMesa,
                          hint: Text("Escolha a mesa"),
                          iconSize: 30,
                          elevation: 1,
                          isDense: false,
                          isExpanded: true,
                          onChanged: (valueMesa) {
                            selecionadoMesa = valueMesa;
                            setState(() {

                            });
                          },
                        ),
                      ),
                      SizedBox(
                        width: 10,
                      ),
                      Expanded(
                        child: DropdownButton(
                          value: selecionadoPessoa,
                          items: listPessoa,
                          hint: Text("Pessoas"),
                          iconSize: 30,
                          isDense: false,
                          isExpanded: true,
                          elevation: 1,
                          onChanged: (valuePessoa) {
                            selecionadoPessoa = valuePessoa;
                            setState(() {

                            });
                          },
                          //),
                        ),
                      ),
                    ],
                  ),
                ),
                SizedBox(
                  height: 10,
                ),
                DropdownButton(
                  value: tipoCategoria,
                  items: listCate,
                  hint: Text("Categoria"),
                  iconSize: 30,
                  isDense: false,
                  isExpanded: true,
                  elevation: 1,
                  onChanged: (valueCategoria) {
                    tipoCategoria = valueCategoria;
                    setState(() {

                    });
                  },
                ),
                SizedBox(
                  height: 10,
                ),
                DropdownButton(
                  value: selecionadoProduto,
                  items: listProduto,
                  hint: Text("Produto"),
                  iconSize: 30,
                  isDense: false,
                  isExpanded: true,
                  elevation: 1,
                  onChanged: (valueProduto) {
                    selecionadoProduto = valueProduto;
                    setState(() {

                    });
                  },
                ),
                SizedBox(
                  height: 10,
                ),
                DropdownButton(
                  value: selecionadoQtde,
                  items: listQtde,
                  hint: Text("Quantidade"),
                  iconSize: 30,
                  isDense: false,
                  isExpanded: true,
                  elevation: 1,
                  onChanged: (valueQtde) {
                    selecionadoQtde = valueQtde;
                    setState(() {

                    });
                  },
                ),
                SizedBox(
                  height: 10,
                ),
                Padding(
                  padding: EdgeInsets.all(0),
                  child: RaisedButton(
                    color: Colors.green,
                    padding: EdgeInsets.fromLTRB(20, 5, 20, 5),
                    child: Row(
                      mainAxisSize: MainAxisSize.min,
                      children: <Widget>[
                        Padding(
                          padding: const EdgeInsets.all(1),
                          child: Icon(
                            Icons.exposure_plus_1,
                            color: Colors.white,
                          ),
                        ),
                        Padding(
                          padding: const EdgeInsets.all(2.0),
                          child: Text(
                            "Adicionar",
                            style: TextStyle(
                              fontSize: 18,
                              color: Colors.white,
                              fontWeight: FontWeight.bold,
                            ),
                          ),
                        ),
                      ],
                    ),
                    onPressed: addPedido,
                  ),
                ),
                SizedBox(
                  height: 10,
                ),
                Divider(
                  height: 5.0,
                ),
                SizedBox(
                  height: 10,
                ),
                Expanded(
                  child: ListView.builder(
                      // ignore: missing_return
                      itemCount: _listaPedido.length,
                      itemBuilder: (context, index) {
                          return ListTile(
                            dense: true,
                            key: Key(DateTime.now().millisecondsSinceEpoch.toString()),
                            title: Text(_listaPedido[index]["produto"] + " Qtde: " + _listaPedido[index]["quantidade"], style: TextStyle(fontSize: 16),
                            ),
                            trailing: GestureDetector(
                              child: Icon(Icons.delete_forever, color: Colors.red,),
                              onTap: () {
                                showDialog(
                                  context: context,
                                  builder: (BuildContext context) {
                                    return AlertDialog(
                                      title:
                                      new Text("Deseja remover a item da lista?"),
                                      actions: <Widget>[
                                        new FlatButton(
                                          color: Colors.red,
                                          child: new Text("Cancelar"),
                                          onPressed: () {
                                            Navigator.of(context).pop();
                                          },
                                        ),
                                        new FlatButton(
                                          color: Colors.green,
                                          child: new Text("Excluir"),
                                          onPressed: () {
                                            _listaPedido.removeAt(index);
                                            setState(() {
                                              Navigator.of(context).pop();
                                            });
                                          },
                                        ),
                                      ],
                                    );
                                  },
                                );
                              },
                            ),
                          );
                      },
                  ),
                ),
                SizedBox(
                  height: 20,
                ),
                Row(
                  mainAxisAlignment: (MainAxisAlignment.center),
                  children: <Widget>[
                    Padding(
                      padding: EdgeInsets.all(0),
                      child: RaisedButton(
                          color: Colors.redAccent,
                          padding: EdgeInsets.all(8),
                          child: Row(
                            mainAxisSize: MainAxisSize.min,
                            children: <Widget>[
                              Padding(
                                padding: const EdgeInsets.all(4.0),
                                child: Icon(
                                  Icons.delete,
                                  color: Colors.white,
                                ),
                              ),
                              Padding(
                                padding: const EdgeInsets.all(2.0),
                                child: Text(
                                  "Limpar",
                                  style: TextStyle(
                                    color: Colors.white,
                                    fontWeight: FontWeight.bold,
                                  ),
                                ),
                              ),
                            ],
                          ),
                          onPressed: () async {
                            ConfirmAction action = await CustomDialog(context,
                                "Deseja remover todos os \n itens?");
                            if (action == ConfirmAction.ACCEPT) {
                              _apagarItensPedido();
                            }
                          }),
                    ),
                    SizedBox(
                      width: 30,
                    ),
                    Padding(
                      padding: EdgeInsets.all(0),
                      child: RaisedButton(
                        color: Colors.green,
                        padding: EdgeInsets.all(8),
                        child: Row(
                          mainAxisSize: MainAxisSize.min,
                          children: <Widget>[
                            Padding(
                              padding: const EdgeInsets.all(4.0),
                              child: Icon(
                                Icons.send,
                                color: Colors.white,
                              ),
                            ),
                            Padding(
                              padding: const EdgeInsets.all(2.0),
                              child: Text(
                                "Enviar",
                                style: TextStyle(
                                  color: Colors.white,
                                  fontWeight: FontWeight.bold,
                                ),
                              ),
                            ),
                          ],
                        ),
                        onPressed: (){
                          enviarDados();
                        },
                      ),
                    ),
                  ],
                ),
              ],
            ),
      );
  //  );
  }
}
