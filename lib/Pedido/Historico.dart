import 'package:flutter/material.dart';

class Historico extends StatefulWidget {
  List dados;

  Historico({Key key, this.dados}) : super(key: key);

  @override
  _HistoricoState createState() => _HistoricoState();
}

class _HistoricoState extends State<Historico> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text("Histórico"),
          backgroundColor: Color(0xff36B3C9),
        ),
        body: Center(
          child: SwipeList(
            dados: widget.dados,
          ),
        ));
  }
}

class SwipeList extends StatefulWidget {
  List dados;

  SwipeList({Key key, this.dados}) : super(key: key);

  State<StatefulWidget> createState() {
    return ListItemWidget(dados: dados);
  }
}

class ListItemWidget extends State<SwipeList> {
  List dados;
  List dadosPedidos;

  ListItemWidget({Key key, this.dados});

  DetalhesPedido() {
    print(widget.dados);
    print(dados);

    dadosPedidos.add(dados);

    print(dadosPedidos);
  }

  Widget build(BuildContext context) {
    return Container(
      child: ListView.builder(
          physics: AlwaysScrollableScrollPhysics(),
          itemCount: dados == null ? 0 : dados.length,
          // ignore: missing_return
          itemBuilder: (BuildContext context, index) {
            return InkWell(
              onTap: () {
                DetalhesPedido();
              },
              child: Card(
                clipBehavior: Clip.antiAlias,
                elevation: 5,
                child: Container(
                  height: 120,
                  child: Row(
                    children: [
                      Container(
                        alignment: Alignment.bottomCenter,
                        //height: 230,
                        width: 100,
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.only(
                            bottomLeft: Radius.circular(5),
                            topLeft: Radius.circular(5),
                          ),
                          image: DecorationImage(
                            alignment: Alignment.center,
                            image: AssetImage("imagens/pedido.png"),
                          ),
                        ),
                      ),
                      Container(
                        height: 130,
                        child: Padding(
                          padding: EdgeInsets.fromLTRB(10, 1, 0, 0),
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Text(
                                "Pedido: " + dados[index]["pedido"],
                                style: TextStyle(
                                    fontSize: 16, fontWeight: FontWeight.bold),
                              ),
                              Padding(
                                padding: EdgeInsets.fromLTRB(0, 3, 0, 3),
                                child: Container(
                                  child: Text(
                                    "Mesa: " + dados[index]["mesa"],
                                  ),
                                ),
                              ),
                              Padding(
                                padding: EdgeInsets.fromLTRB(0, 3, 0, 3),
                                child: Container(
                                  child: Text(
                                    "Clientes: " + dados[index]["pessoa"],
                                  ),
                                ),
                              ),
                            ],
                          ),
                        ),
                      )
                    ],
                  ),
                ),
              ),
            );
          }),
    );
  }
}
